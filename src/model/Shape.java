package model;

public abstract class Shape {
    protected String color;
    protected boolean fiiled;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFiiled() {
        return fiiled;
    }

    public void setFiiled(boolean fiiled) {
        this.fiiled = fiiled;
    }

    public Shape(String color, boolean fiiled) {
        this.color = color;
        this.fiiled = fiiled;
    }

    public Shape() {
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    @Override
    public String toString() {
        return "Shape [color=" + color + ", fiiled=" + fiiled + "]";
    }

}
