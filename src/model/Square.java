package model;

public class Square extends Rectangle {

    public Square(String color, boolean fiiled, double side) {
        super(color, fiiled, side, side);

    }

    public Square(double side) {
        super(side, side);
    }

    @Override
    public void setWidth(double side) {

        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {

        super.setLength(side);
    }

    @Override
    public String toString() {
        return "Square []" + " " + super.toString();
    }

}
