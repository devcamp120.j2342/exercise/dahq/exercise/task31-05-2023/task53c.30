import model.Circle;
import model.Rectangle;
import model.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle = new Rectangle(9.5, 5.5);
        Circle circle = new Circle(9);
        Square square = new Square(5);
        Square square2 = new Square("Green", false, 8.6);

        System.out.println("Square: " + square.toString());
        System.out.println("Rectangle: " + rectangle.toString());
        System.out.println("Circle: " + circle.toString());
        System.out.println("Circle: " + square2.toString());
        System.out.println("S Square: " + square.getArea());
        System.out.println("S rectangle: " + rectangle.getArea());
        System.out.println("S Circle: " + circle.getArea());
    }
}
